#!/bin/sh 
set -eux

RDIFFWEB_DATABASE="/etc/rdiffweb/rdw.db"
RDIFFWEB_CONFIG="/etc/rdiffweb/rdw.conf"
RDIFFWEB_HOME="/rdiff-backup"

# get password from file
if [ -n "${INITIAL_SHA1PASS_FILE+x}" ]; then
  INITIAL_SHA1PASS="$(< "$INITIAL_SHA1PASS_FILE")"
fi

if [ "$1" = 'rdiffweb' ]; then
	# set username, password
	if [ -n "$INITIAL_SHA1PASS" ]; then
		if [ -f "$RDIFFWEB_DATABASE" ]; then
			CURRENT_COUNT="$(echo "select count(*) from users where userID=1" | sqlite3 $RDIFFWEB_DATABASE)"
			CURRENT_SHA1PASS="$(echo "select Password from users where userID=1" | sqlite3 $RDIFFWEB_DATABASE)"
			if [ "$CURRENT_COUNT" -eq "1" ]; then
				if [ "$CURRENT_SHA1PASS" != "$INITIAL_SHA1PASS" ]; then
					echo "update users set Password=\"$INITIAL_SHA1PASS\" where userID=1" | sqlite3 $RDIFFWEB_DATABASE
				fi			
			else			
					echo "INSERT INTO users VALUES(1,\"admin\",\"$INITIAL_SHA1PASS\",\"\",0,\"$RDIFFWEB_HOME\",1,0)" | sqlite3 $RDIFFWEB_DATABASE
			fi
		else
			cat <<-EOF | sqlite3 $RDIFFWEB_DATABASE
				PRAGMA foreign_keys=OFF;
				BEGIN TRANSACTION;
				CREATE TABLE users (
				UserID integer primary key autoincrement,
				Username varchar (50) unique NOT NULL,
				Password varchar (40) NOT NULL DEFAULT "",
				UserRoot varchar (255) NOT NULL DEFAULT "",
				IsAdmin tinyint NOT NULL DEFAULT FALSE,
				UserEmail varchar (255) NOT NULL DEFAULT "",
				RestoreFormat tinyint NOT NULL DEFAULT TRUE, role tinyint NOT NULL DEFAULT "10");
				INSERT INTO users VALUES(1,"admin","$INITIAL_SHA1PASS","",0,"$RDIFFWEB_HOME",1,0);

				CREATE TABLE repos (
				RepoID integer primary key autoincrement,
				UserID int(11) NOT NULL,
				RepoPath varchar (255) NOT NULL,
				MaxAge tinyint NOT NULL DEFAULT 0,
				Encoding varchar (50), keepdays varchar(255) NOT NULL DEFAULT "");

				CREATE TABLE sshkeys (
				Fingerprint primary key,
				Key clob UNIQUE,
				UserID int(11) NOT NULL);
				DELETE FROM sqlite_sequence;
				INSERT INTO sqlite_sequence VALUES('users',2);
				INSERT INTO sqlite_sequence VALUES('repos',1);
				COMMIT;
			EOF
		fi
	fi

	# set home
	CURRENT_HOME="$(echo "select UserRoot from users where userID=1" | sqlite3 $RDIFFWEB_DATABASE)"
	if [ "$CURRENT_HOME" != "$RDIFFWEB_HOME" ]; then
		echo "update users set UserRoot=\"$RDIFFWEB_HOME\" where userID=1" | sqlite3 $RDIFFWEB_DATABASE
	fi			

	# set theme, header name
	if [ -n "$INITIAL_THEME" ]; then
		sed -i -e "s/^#\?DefaultTheme=.*/DefaultTheme=${INITIAL_THEME}/" $RDIFFWEB_CONFIG
	fi		
	
	if [ -n "$INITIAL_HEADER" ]; then
		sed -i -e "s/^#\?HeaderName=.*/HeaderName=${INITIAL_HEADER}/" $RDIFFWEB_CONFIG
	fi		
	
	exec /usr/bin/rdiffweb "$@"
fi

exec "$@"
